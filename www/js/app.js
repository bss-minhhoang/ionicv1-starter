// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.run(function($ionicPlatform, $timeout, $rootScope) {
  $ionicPlatform.ready(function() {
    // if(window.cordova && window.cordova.plugins.Keyboard) {
    //   // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    //   // for form inputs)
    //   cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    //
    //   // Don't remove this line unless you know what you are doing. It stops the viewport
    //   // from snapping when text inputs are focused. Ionic handles this internally for
    //   // a much nicer keyboard experience.
    //   cordova.plugins.Keyboard.disableScroll(true);
    // }
    // if(window.StatusBar) {
    //   StatusBar.styleDefault();
    // }

      $rootScope.test =  function(){
          console.log('-------- Start operation');
          checkForUpdate();
      }

      var checkForUpdate = function() {
          var options = {
              'config-file': 'http://192.168.1.31/config/ionicv1-starter/www/chcp.json'
          };
          chcp.fetchUpdate(fetchUpdateCallback,options);
      };

      var fetchUpdateCallback = function(error, data) {
          if (error) {
              console.log('Failed to load the update with error code: ' + error.code);
              console.log(error.description);
              return;
          }
          console.log('Update is loaded, running the installation');

          chcp.installUpdate(installationCallback);
      };

      var installationCallback =  function(error) {
          if (error) {
              console.log('Failed to install the update with error code: ' + error.code);
              console.log(error.description);
          } else {
              console.log('Update installed!');
          }
      };
  });
});
